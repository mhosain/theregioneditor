package region_editor;
 
import java.io.File;
import javafx.application.Application;
import javafx.stage.Stage;
import static region_editor.RegionEditorConstants.FILE_WORLD_SCHEMA;
import region_editor.view.RegionEditorView;
import world_data.WorldDataManager;
import world_io.WorldIO;

/**
 * This class is the entry point for our application. It sets
 * up the Data and File Managers and starts the GUI.
 */
public class RegionEditor extends Application {
    /**
     * This method initializes the app and starts it.
     * 
     * @param primaryStage The application window.
     */    
    @Override	    
    public void start(Stage primaryStage) {
	// MAKE THE DATA MANAGER
	WorldDataManager worldDataManager = new WorldDataManager();
	
	// INIT THE FILE I/O
        // AND OUR IMPORTER/EXPORTER
        File schemaFile = new File(FILE_WORLD_SCHEMA);
        WorldIO worldIO = new WorldIO(schemaFile);
        worldDataManager.setWorldImporterExporter(worldIO);
		
	// MAKE THE USER INTERFACE, WHICH ALSO STARTS IT UP
	RegionEditorView gui = new RegionEditorView(primaryStage, 
						    worldDataManager);
        gui.enableRegionEditor(false);
        
        
       
        
       
     
        
    }

    /**
     * Here is where our application starts. This just launches
     * the JavaFX app, which will initialize the ui and start it up.
     * 
     * @param args Command-line arguments, which we won't be using.
     */
    public static void main(String[] args)
    {
	launch(args);
        
        
        
    }
}
